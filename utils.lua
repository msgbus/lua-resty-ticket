_M = {}

local function split(inputstr, sep)
    if sep == nil then
        sep = "%s"
    end
    local t = {}
    local i = 1
    for str in string.gmatch(inputstr, "([^"..sep.."]+)") do
        t[i] = str
        i = i + 1
    end
    return t
end

local function deal_invalid_json_string(json_str)
    if nil == json_str or "" == json_str then
        return false
    end
    local json_str = json_str:gsub("[%s{}]", "")
    local kv = split(json_str, ",")
    for _, s in pairs(kv) do
        repeat
            if nil == s:find("\"a\"") then
                break
            end
            local pair = split(s, ":")
            if table.getn(pair) ~= 2 then
                break
            end
            local appkey = pair[2]
            return true, appkey
        until true
    end
    return false
end

local function remove_duplicate(t)
    local hash = {}
    local res = {}

    for _,v in ipairs(t) do
        if (not hash[v]) then
            res[#res+1] = v -- you could print here instead of saving to result table if you wanted
            hash[v] = true
        end
    end
    return res
end

_M.split = split
_M.deal_invalid_json_string = deal_invalid_json_string
_M.remove_duplicate = remove_duplicate

return _M
