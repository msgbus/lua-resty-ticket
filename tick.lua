local cjson = require "cjson"
local utils = require "utils"

local appkey
local default_broker = "tcp://182.92.154.3:1883" -- abj-front-1
local config = ngx.shared.config
local json_data = ngx.req.get_body_data()
local ok, content = pcall(cjson.decode, json_data);

if not ok then
    ok, appkey = utils.deal_invalid_json_string(json_data)
else
    appkey = content["a"]
end

function dispatch(appkey)
    local index = config:get(appkey.."_i")
    local priority_sum = config:get(appkey.."_sum")
    local broker, format_broker

    if index ~= nil and priority_sum ~= nil then
        if index > priority_sum then
            index = 1
        end
        if index + 1 <= priority_sum then
            config:set(appkey.."_i", index + 1);    -- increase the index
        else
            config:set(appkey.."_i", 1);            -- reset the index
        end

        broker = config:get(appkey.."_"..index)
        if broker == nil then
            broker = default_broker
        end
    else
        broker = default_broker
        ngx.log(ngx.ERR, "dipatch default broker abj-front-1")
    end

    local format_broker = string.gsub(cjson.encode({c = broker}), "\\/", "/")
    ngx.header.content_type = "application/json"
    ngx.header["Content-Length"] = format_broker:len()
    ngx.print(format_broker)
end

if config:get(appkey) == nil then
    dispatch("normal")
else
    dispatch(appkey)
end
