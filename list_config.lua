local cjson = require "cjson"
local utils = require "utils"

local config = ngx.shared.config
local all_config = {}

for _, s in pairs(config:get_keys()) do
    if string.find(s, "_i", 0) == nil then
        broker_list = utils.split(config:get(s), "$$")
        table.insert(all_config, {s, broker_list})
    end
end

local rep = string.gsub(cjson.encode(all_config), "\\/", "/")
ngx.header.content_type = "application/json"
ngx.header["Content-Length"] = rep:len()
ngx.print(rep)
