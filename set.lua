local cjson = require "cjson"
local utils = require "utils"

local data = ngx.req.get_body_data()
local config = ngx.shared.config
local content = cjson.decode(data)

if content ~= nil then
    for appkey, broker_list in pairs(content) do
        local index = 1
        local sum = 0

        for _, broker in pairs(broker_list) do
            local p = utils.split(broker, "#")
            local host = p[1]
            local priority = tonumber(p[2])
            for i = 1, priority do
                config:set(appkey.."_"..index, host)
                index = index + 1
            end
            sum = sum + priority
        end

        if config:get(appkey.."_i") == nil then
            config:set(appkey.."_i", 1)
        end

        if sum == 0 then
            config:delete(appkey)
        else
            config:set(appkey, "true")
        end
        config:set(appkey.."_sum", sum)

        ngx.say(config:get(appkey))
    end
else
    ngx.say("error")
end
